//package com.test.bmkp.androidtest;
//
//import android.app.Application;
//import android.content.Context;
//import android.os.Environment;
//import android.util.Log;
//
//import com.alipay.euler.andfix.patch.PatchManager;
//
//import cn.jiajixin.nuwa.Nuwa;
//
///**
// * Created by sks on 2016/3/9.
// */
//public class MainApplication extends Application {
//
//    private static final String APATCH_PATH = "/out.apatch";
//    private PatchManager patchManager;
//    @Override
//    public void onCreate() {
//        super.onCreate();
//
////        initAndFix();
//
//    }
//
//
//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//
//        initNuWa();
//
//    }
//
//    private void initNuWa(){
//
//        Nuwa.init(this);
//        Nuwa.loadPatch(this,"/sdcard/patch.jar");
//
//    }
//
//    private void initAndFix(){
//
//        patchManager = new PatchManager(this);
//
//        try{
//
//            String version = getPackageManager().getPackageInfo(getPackageName(),0).versionName;
//
//            Log.d("zhangn", "version ===>"+version);
//
//            patchManager.init(version);
//
//            patchManager.loadPatch();  // 加载已缓存的补丁
//
//
//        }catch(Exception e) {
//
//            Log.d("zhangn", "===>"+e.getMessage());
//        }
//    }
//
//    public void addPatch(){
//
//        try{
//
//            String patchFileString = Environment.getExternalStorageDirectory()
//                    .getAbsolutePath() + APATCH_PATH;
//
//            patchManager.addPatch(patchFileString);
//
//            Log.d("zhangn", "apatch:" + patchFileString + " added.");
//
//        }catch(Exception e) {
//
//            Log.d("zhangn", "===>"+e.getMessage());
//        }
//
//    }
//}
