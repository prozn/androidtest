//package com.test.bmkp.androidtest.RxJava;
//
//import rx.Observable;
//import rx.Observer;
//import rx.Subscriber;
//
///**
// * Created by sks on 2016/5/4.
// */
//public class RxJavaObserver {
//
//
//    /**
//     * 观察者
//     */
//    Observer<String> observer = new Observer<String>() {
//        @Override
//        public void onCompleted() {
//
//        }
//
//        @Override
//        public void onError(Throwable e) {
//
//        }
//
//        @Override
//        public void onNext(String s) {
//
//        }
//    };
//
//
//    /**
//     * 订阅者  implement Observer
//     */
//    Subscriber<String> subscriber = new Subscriber<String>() {
//        @Override
//        public void onCompleted() {
//
//        }
//
//        @Override
//        public void onError(Throwable e) {
//        }
//
//        @Override
//        public void onNext(String s) {
//
//        }
//    };
//
//
//
//
//    public void test(){
//
//        subscriber.isUnsubscribed(); // 是否取消订阅
//        subscriber.unsubscribe();// 取消订阅
//
//        subscriber.onStart();
//
//
//        /**
//         * 被观察者
//         *
//         * onSubscribe 订阅表，来确定订阅者的执行序列
//         */
//        Observable observable = Observable.create(new Observable.OnSubscribe<String>() {
//            @Override
//            public void call(Subscriber<? super String> subscriber) {
//
//                subscriber.onNext("hello");
//
//                subscriber.onNext("world");
//
//                subscriber.onCompleted();
//
//            }
//        });
//    }
//}
