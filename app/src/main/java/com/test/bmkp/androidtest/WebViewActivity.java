package com.test.bmkp.androidtest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

@SuppressLint("SetJavaScriptEnabled")
public class WebViewActivity extends AppCompatActivity {

    WebView mWebView;
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        initView();
    }


    private void initView(){
        mWebView = (WebView)findViewById(R.id.webview);
        btn = (Button)findViewById(R.id.btn);

        //设置编码
        mWebView.getSettings().setDefaultTextEncodingName("utf-8");
        //支持js
        mWebView.getSettings().setJavaScriptEnabled(true);
        //设置背景颜色 透明
        mWebView.setBackgroundColor(Color.argb(0, 0, 0, 0));
        //设置本地调用对象及其接口
        mWebView.addJavascriptInterface(new JavaScriptObject(WebViewActivity.this), "myObj");
        //载入js
        mWebView.loadUrl("file:///android_asset/test.html");

        //点击调用js中方法
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mWebView.loadUrl("javascript:funFromjs()");
            }
        });
    }

    public class JavaScriptObject {
        Context mContxt;

        public JavaScriptObject(Context mContxt) {
            this.mContxt = mContxt;
        }

        @JavascriptInterface
        public void fun1FromAndroid() {
            Log.d("zhangn", "======>form android");
            Toast.makeText(mContxt, "调用fun2:", Toast.LENGTH_SHORT).show();
        }

        @JavascriptInterface
        public void fun2(String name) {
            Toast.makeText(mContxt, "调用fun2:" + name, Toast.LENGTH_SHORT).show();
        }
    }
}
