package com.test.bmkp.androidtest.partten;

/**
 * Created by sks on 2016/4/1.
 */
public class Student {

    private String age;

    private String phone;

    private String name;

    public Student(Builder builder) {
        age = builder.age;
        phone = builder.phone;
        name = builder.name;
    }



    public static class Builder{

        private String age;

        private String phone;

        private String name;


       public Builder age(String age){

           this.age = age;
           return this;

       }

        public Builder phone(String phone){
            this.phone = phone;
            return this;
        }

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public Student build(){

            return new Student(this);
        }


    }

}
