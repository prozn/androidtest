package com.example;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by sks on 2016/4/21.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface Bind {

    public String id() default "";

    public String name() default "";
}
