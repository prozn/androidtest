package com.example;

import java.lang.reflect.Field;

/**
 * Created by sks on 2016/4/21.
 */
public class Main {



    public static void  main(String[] args) throws Exception{

        Class<?> testClass = Class.forName("com.example.MyClass");

        Field[] fields = testClass.getDeclaredFields();

        for(Field field : fields) {

            System.out.println(field.getName());

            Bind bind = field.getAnnotation(Bind.class);

            if(bind != null) {

                System.out.println(bind.id());

                System.out.println(bind.name());
            }
        }
    }
}
