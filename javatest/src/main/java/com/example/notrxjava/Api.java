package com.example.notrxjava;

import com.sun.jndi.toolkit.url.Uri;

import java.util.List;

/**
 * Created by sks on 2016/5/9.
 */
public interface Api {


    // 塞式设计
    List<Cat> queryCats(String query);  // 查询

    Uri store(Cat cat);             // 存储
}
