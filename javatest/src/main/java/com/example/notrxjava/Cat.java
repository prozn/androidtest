package com.example.notrxjava;

public class Cat implements Comparable<Cat> {


    String name;
    int cuteness; // 度


    @Override
    public int compareTo(Cat another) {
        return cuteness > another.cuteness ? 1 : -1;
    }
}
