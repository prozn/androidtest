package com.example.notrxjava;

/**
 * Created by sks on 2016/5/9.
 */
public interface Fun<T, R> {

    R call(T t);
}
