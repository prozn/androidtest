package com.example.obj;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by sks on 2016/5/9.
 */
public class ObserverA implements Observer {


    /**
     * 注册观察者
     * @param observable
     */
    public void registerSubject(Observable observable) {

        observable.addObserver(this);
    }




    @Override
    public void update(Observable o, Object arg) {



    }
}
