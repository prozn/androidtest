package com.example.obj;

import java.util.Observable;

/**
 * Created by sks on 2016/5/9.
 */
public class SubjectA extends Observable {


    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;

        setChanged();
        notifyObservers("");
    }
}
